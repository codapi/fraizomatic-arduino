#include <OneWire.h>
#include <DallasTemperature.h>
#include <DHT.h>
#include <Arduino_JSON.h>

#define PLANTER_ID 1
#define DS18B20 2

#define DHTTYPE DHT11
#define HUMIDITY_PIN 3
#define LIGHT_SENSOR_PIN A1
#define LED_PIN 10
#define SOLENOID_PIN 11

#define TEMPERATURE_DELAY_S 5
#define HUMIDITY_DELAY_S 5
#define LIGHT_DELAY_S 5
#define SERVER_CALL_DELAY_S 15

float sum_temperature = 0;
int count_temperature = 0;
float sum_humidity = 0;
int count_humidity = 0;
float sum_light = 0;
int count_light = 0;
unsigned long last_temperature_time = 0;
unsigned long last_humidity_time = 0;
unsigned long last_light_time = 0;
unsigned long last_server_call_time = 0;

OneWire temperature_wire(DS18B20);
DallasTemperature temperature_sensor(&temperature_wire);
DHT humidity_sensor(HUMIDITY_PIN, DHTTYPE);

void setup() {
  Serial.begin(9600);
  pinMode(HUMIDITY_PIN, INPUT);
  pinMode(SOLENOID_PIN, OUTPUT);
  pinMode(LED_PIN, OUTPUT);
  temperature_sensor.begin();
  humidity_sensor.begin();
}

void loop() {
  unsigned long current_time = millis();

  delay(50);

  if(last_server_call_time + ((unsigned long)SERVER_CALL_DELAY_S) * ((unsigned long)1000) < current_time) {
   JSONVar server_result = sendToServer(); 
   if (server_result != "undefined") {
    if (server_result.hasOwnProperty("light") && server_result.hasOwnProperty("solenoid")) {
      updateActioneurs((int)server_result["light"], (int)server_result["solenoid"]);
    }
    Serial.print(JSON.stringify(server_result));
   }
   
   resetValues();
   last_server_call_time = current_time;
  }

  if(last_temperature_time + ((unsigned long)TEMPERATURE_DELAY_S) * ((unsigned long)1000) < current_time) {
    int temperature = getTemperature();
    updateTemperature(temperature);
    last_temperature_time = current_time;
  }

  if(last_humidity_time + ((unsigned long)HUMIDITY_DELAY_S) * ((unsigned long)1000) < current_time) {
    int humidity = getHumidity();
    updateHumidity(humidity);
    last_humidity_time = current_time;
  }

  if(last_light_time + ((unsigned long)LIGHT_DELAY_S) * ((unsigned long)1000) < current_time) {
    int light = getLight();
    updateLight(light);
    last_light_time = current_time;
  }
 
}

int getTemperature()
{
  temperature_sensor.requestTemperatures();
  return temperature_sensor.getTempCByIndex(0);
}

float getAvgTemperature()
{
  if(count_temperature == 0) {
    return 0;
  }
  return (float)sum_temperature/(float)count_temperature;
}


void updateTemperature(int newValue)
{
  sum_temperature += newValue;
  count_temperature++;
}

int getHumidity()
{
  float humidity = humidity_sensor.readHumidity();
  while(isnan(humidity)) {
    humidity = humidity_sensor.readHumidity();
    delay(300);
  }
  return humidity;
}

float getAvgHumidity()
{
  if(count_humidity == 0) {
    return 0;
  }
  return (float)sum_humidity/(float)count_humidity;
}


void updateHumidity(int newValue)
{
  sum_humidity += newValue;
  count_humidity++;
}


int getLight()
{
  return analogRead(LIGHT_SENSOR_PIN);
}

float getAvgLight()
{
  if(count_light == 0) {
    return 0;
  }
  return (float)sum_light/(float)count_light;
}

void updateLight(int newValue)
{
  sum_light += newValue;
  count_light++;
}


void resetValues()
{
  sum_temperature = 0;
  count_temperature = 0;
  sum_humidity = 0;
  count_humidity = 0;
  sum_light = 0;
  count_light = 0;
}

void updateActioneurs(int light, int solenoid)
{
  if(solenoid > 0) {
    digitalWrite(SOLENOID_PIN, HIGH);
  } else {
    digitalWrite(SOLENOID_PIN, LOW);
  }

  if(light > 0) {
    digitalWrite(LED_PIN, HIGH);
  } else {
    digitalWrite(LED_PIN, LOW);
  }
}

JSONVar sendToServer()
{
  String requestStr = "HTTP ";
  requestStr.concat(PLANTER_ID);
  requestStr.concat(" ");
  requestStr.concat(getAvgTemperature());
  requestStr.concat(" ");
  requestStr.concat(getAvgHumidity());
  requestStr.concat(" ");
  requestStr.concat(getAvgLight());
  Serial.println(requestStr);
  
  String rcv = "";
  while(rcv.compareTo("") == 0) {
    rcv = Serial.readString();
  }
  return JSON.parse(rcv);
}
