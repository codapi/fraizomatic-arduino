import serial
import time
import requests

URL = 'http://10.44.19.197:8000/api/send_information'

arduino = serial.Serial(port='COM5', baudrate=9600, timeout=.1)

if __name__ == '__main__':
    while(True):
        data = arduino.readline()
        if b'HTTP' in data:
            data = data.decode()
            if data and "\n" in data and "HTTP" in data:
                values = data.replace("\n", "").replace("\r", "").split(" ")
                payload = {'hygrometry': values[3], 'temperature': values[2], 'brightness': values[4], 'planter': values[1]}
                try:
                    print(payload)
                    r = requests.post(URL, data=payload, timeout=5)
                    if r.status_code == 200:
                        arduino.write(bytes(r.text.replace("\n", ""), 'utf-8'))
                    else:
                        print("Invalide code : "+str(r.status_code))
                except Exception as e:
                    arduino.write(b'no value')
                    print(str(e))
                # arduino.write(json.dumps(payload).encode('utf-8'))
                time.sleep(0.1)
        elif data != b'':
            print("---"+str(data)+"---")